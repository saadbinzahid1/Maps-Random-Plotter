import mercator
from osgeo import ogr
import random


def random_markers(edge_points,zoom_level=22,no_of_markers=15):
    #Params
    #edge_points => Must be closed boundry i.e first and last points must be same.
    #zoom_level controls the resolution of the map.
    #no_of_markers is the no of coordinates produced in the output.
    #Each tiles has a size of 256 x 256 pixels on the viewport at any zoom level.

    # Create ring
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for i in range(0,len(edge_points)):
        ring.AddPoint(edge_points[i][0],edge_points[i][1])
    # Create polygon
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    wkt =poly.ExportToWkt()
    geom = ogr.CreateGeometryFromWkt(wkt)
    # Get Envelope returns a tuple (minX, maxX, minY, maxY)
    env = geom.GetEnvelope()
    #print(env)
    #print ("minX: %d, minY: %d, maxX: %d, maxY: %d" %(env[0],env[2],env[1],env[3]))
    sw = [env[0],env[2]]
    ne = [env[1],env[3]]
    sw_x,sw_y = mercator.get_tile_at_lat_lng(sw[0],sw[1],zoom_level)
    ne_x,ne_y = mercator.get_tile_at_lat_lng(ne[0],ne[1],zoom_level)

    delta = ne_x - sw_x

    nw_x = ne_x - delta
    nw_y = ne_y

    se_x = sw_x + delta
    se_y = sw_y

    tiles_in_box = []
    for i in range(nw_x,se_x+1):
        for j in range(nw_y,se_y+1):
            tiles_in_box.append((i,j))
    #print(tiles_in_box)

    tiles_in_polygon = []
    for i in range(0,len(tiles_in_box)):
        bounds = mercator.get_tile_bounds(tiles_in_box[i][0],tiles_in_box[i][1],zoom_level)
        c_x = (bounds[0][0] + bounds[1][0])/2
        c_y = (bounds[0][1] + bounds[1][1])/2
        point1 = ogr.Geometry(ogr.wkbPoint)
        point1.AddPoint(c_x,c_y)
        if(poly.Contains(point1)):
            tiles_in_polygon.append((c_x,c_y))
    #print(tiles_in_polygon)

    random.shuffle(tiles_in_polygon)            #uses fisher-yates shuffle algorithm.

    k = len(tiles_in_polygon)
    if(no_of_markers>k):
        no_of_markers = k

    markers = []
    for i in range(0,no_of_markers):
        markers.append(tiles_in_polygon[i])

    #print(markers)
    #for i in range(0,len(markers)):
    #    print(markers[i][0],markers[i][1])
    return markers



boundry = [(33.666536,73.040448),(33.666619, 73.040585),(33.666661, 73.040538),(33.666727,73.040816),(33.666280,73.041172)
    ,(33.666074, 73.040759)
    ,(33.666536,73.040448)]
print(random_markers(boundry))
