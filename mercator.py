import math

def get_tile_box(zoom, x, y):
    """convert Google-style Mercator tile coordinate to
    (minlat, maxlat, minlng, maxlng) bounding box"""

    minlat, minlng = get_tile_lat_lng(zoom, x, y + 1)
    maxlat, maxlng = get_tile_lat_lng(zoom, x + 1, y)

    return (minlat, maxlat, minlng, maxlng)

def get_lat_lng_of_tile(zoom, x, y):
    """convert Google-style Mercator tile coordinate to
    (lat, lng) of top-left corner of tile"""

    # "map-centric" latitude, in radians:
    lat_rad = math.pi - 2*math.pi*y/(2**zoom)
    # true latitude:
    lat_rad = gudermannian(lat_rad)
    lat = lat_rad * 180.0 / math.pi

    # longitude maps linearly to map, so we simply scale:
    lng = -180.0 + 360.0*x/(2**zoom)

    return (lat, lng)

def from_lat_lng_to_point(lat,lng):
    siny =  min(max(math.sin(lat* (math.pi / 180)),-.9999),.9999)
    x= 128 + lng * (256/360)
    y= 128 + 0.5 * math.log((1 + siny) / (1 - siny)) * -(256 / (2 * math.pi))
    return(x,y)
def from_point_to_lat_lng(x,y):
    lat = (2 * math.atan(math.exp((y - 128) / -(256 / (2 * math.pi)))) - math.pi / 2)/ (math.pi / 180)
    lng = (x - 128) / (256 / 360)
    return (lat,lng)

def get_tile_at_lat_lng(lat, lng, zoom):
    """convert lat/lng to Google-style Mercator tile coordinate (x, y)
    at the given zoom level"""
    t=math.pow(2,zoom)
    s=256/t
    x,y=from_lat_lng_to_point(lat,lng)
    x = math.floor(x/s)
    y= math.floor(y/s)
    return (x, y)
def get_tile_bounds(x,y,z):
    t=math.pow(2,z)
    x=((x%t)+t)%t
    y=((y%t)+t)%t
    s=256/t
    x1=x*s
    y1=(y*s)+s
    x2=x*s+s
    y2=(y*s)
    return[from_point_to_lat_lng(x1,y1),from_point_to_lat_lng(x2,y2)]

def gudermannian(x):
    return 2*math.atan(math.exp(x)) - math.pi/2

def inv_gudermannian(y):
    return math.log(math.tan((y + math.pi/2) / 2))