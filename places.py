from googleplaces import GooglePlaces, types, lang
import math
import maps

def toRad(a):
   return (float(a) * math.pi / 180)

def toDeg(b):
   return (float(b) * 180 / math.pi)

def dst_point(brng, dist,lat,lng):
   dist = dist / 6371  
   brng = toRad(brng)

   lat1 = toRad(lat)
   lon1 = toRad(lng)

   lat2 = math.asin(math.sin(lat1) * math.cos(dist) + math.cos(lat1) * math.sin(dist) * math.cos(brng))

   lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(dist) * math.cos(lat1), math.cos(dist) - math.sin(lat1) * math.sin(lat2))


   return (toDeg(lat2), toDeg(lon2))

YOUR_API_KEY = 'AIzaSyD3bXmmzIs4vB9LwfojJg4-LdBJnWw0DFY'

google_places = GooglePlaces(YOUR_API_KEY)

# You may prefer to use the text_search API, instead.
query_result = google_places.nearby_search(
        location='Islamabad, Pakistan', keyword='Museum',
        radius=10000, types=[types.TYPE_MUSEUM])
# If types param contains only 1 item the request to Google Places API
# will be send as type param to fullfil:
# http://googlegeodevelopers.blogspot.com.au/2016/02/changes-and-quality-improvements-in_16.html

#if query_result.has_attributions:
#    print (query_result.html_attributions)

locations=[]
for place in query_result.places:
    # Returned places from a query are place summaries.
    #print (place.name)
    #print (place.geo_location)
    locations.append(place.geo_location)

    #print (place.place_id)
    # The following method has to make a further API call.
    place.get_details()
    # Referencing any of the attributes below, prior to making a call to
    # get_details() will raise a googleplaces.GooglePlacesAttributeError.
    #print (place.details) # A dict matching the JSON response from Google.
    #print (place.local_phone_number)
    #print (place.international_phone_number)
    #print (place.website)
    #print (place.url)
print(locations)
for i in range(0,len(locations)):
    n = dst_point(0,0.15,locations[i]['lat'],locations[i]['lng'])
    e = dst_point(90,0.15,locations[i]['lat'],locations[i]['lng'])
    s = dst_point(180,0.15,locations[i]['lat'],locations[i]['lng'])
    w = dst_point(270,0.15,locations[i]['lat'],locations[i]['lng'])
    #print(n,e,s,w)
    bound = [n,e,s,w,n]
    print(maps.random_markers(bound))